-- G�A�R�A�N�T�I�A�S
--------------------
select
	grt_.vdagrtsgl ,
	grt.totcapvlr imvlr,
	grt.prpprmvlr premio,
	pdt.ramcod,
	cnt.vdadmncntdes carac,
	grt.chrqtd vlr
from 
	vnemprpitmgrt grt,
	vnptpdtgrt pdt,
	vnpkgrt grt_,
	vnprgrtcapctc cap,
	outer vnpkdmncnt cnt
 where pdt.vdapdtgrtcod = grt.vdapdtgrtcod
   and pdt.vdapdtvercod = grt.vdapdtvercod
   and grt_.vdagrtcod = pdt.vdagrtcod 
   and cap.vdapdtgrtcod = grt.vdapdtgrtcod
   and cap.vdapdtvercod = grt.vdapdtvercod
   and cnt.vdadmncntcod = cap.chrtip
   --and cap.chrtip >0 and cnt.vdadmncntdes = "Dia(s)"
   and grt.prporg = 7
   and grt.prpnumdig = 38659
 ;
 
























-- QUERIES PARA TESTES
select cnt.vdadmncntdes  from vnpkfrq frq, vnpkdmncnt cnt
 where frq.vdafrqtip = cnt.vdadmncntcod
 ;
 
select cnt.vdadmncntdes from vnptfrqorg frq, vnpkdmncnt cnt
 where frq.vdafrqorgcod = cnt.vdadmncntcod
 ;
 
select * from vnptpdtgrt;
select * from vnemprpitm;
select * from vnemprpitmgrt;

-- G�A�R�A�N�T�I�A�S � C�A�R�A�C�T�E�R�ͷS�T�I�C�A�S
----------------------------------------------------
select grt.prporg , grt.prpnumdig,
   grt_.vdagrtsgl ,
   grt.totcapvlr imvlr,
   grt.prpprmvlr premio,
   pdt.ramcod ramo,
   cnt.vdadmncntdes caracteristica,
   cap.chrtip codigo_carac,
   cap.chrminqtd qtde_min_chr,
   cap.chrmaxqtd qtde_max_chr,
   grt.chrqtd qtde,
   frq.vdafrqdes,
   frq.vdapdtgrtcod produto,
   frq.vdapdtvercod versao,
   frq.vdafrqtip codigo_tipo_franquia,
   frq.vdadmncntdes descricao_tipo_franquia
  from vnemprp prp,
   vnemsgr sgr,
   vnemprpitm itm,
   vnpkdmncnt cnt_prp,
   vnemprpitmgrt grt,
   vnptpdtgrt pdt,
   vnpkgrt grt_,
   vnprgrtcapctc cap,
   outer vnpkdmncnt cnt,
   outer (select frq.vdapdtgrtcod,
                 frq.vdapdtvercod,
                 npk.*, cnt_frq.*
            from vnprgrtfrq frq, vnpkfrq npk, outer vnpkdmncnt cnt_frq
           where frq.vdafrqtabcod = npk.vdafrqtabcod
             and npk.vdafrqtip = cnt_frq.vdadmncntcod) frq
 where prp.vdasgrcod = sgr.vdasgrcod
   and prp.prpsitcod = cnt_prp.vdadmncntcod
   and prp.prporg = itm.prporg 
   and prp.prpnumdig = itm.prpnumdig
   and itm.prporg = grt.prporg 
   and itm.prpnumdig = grt.prpnumdig
   and itm.prpitmnum = grt.prpitmnum
   and pdt.vdapdtgrtcod = grt.vdapdtgrtcod
   and pdt.vdapdtvercod = grt.vdapdtvercod
   and cap.vdapdtgrtcod = grt.vdapdtgrtcod
   and cap.vdapdtvercod = grt.vdapdtvercod
   and grt_.vdagrtcod = pdt.vdagrtcod
   and cnt.vdadmncntcod = cap.chrtip
   and pdt.vdapdtgrtcod = frq.vdapdtgrtcod
   and pdt.vdapdtvercod = frq.vdapdtvercod
   and cap.chrtip >0
   and prp.dctvigfnldat >= "2021-12-16"
   and prp.dctvigfnldat <= "9999-12-31"
   and prp.prporg <> 94
   and cnt_prp.vdadmncntrefcod <> 'CAN'   
   and prp.vdasgrcod not in (select prp1.vdarnvsgrcod
                                from vnemprp prp1
                                     inner join vnpkdmncnt cnt1 on
                                                (prp1.prpsitcod = cnt1.vdadmncntcod)
                               where prp.vdasgrcod = prp1.vdarnvsgrcod
                                 and prp1.prporg = 94
                                 and cnt1.vdadmncntrefcod = 'PEM')
   --and grt.prporg = 7
   --and grt.prpnumdig = 40236
   ;