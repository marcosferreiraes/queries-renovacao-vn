-- S�E�G�U�R�A�D�O
------------------
select 
	seg.rndvlr renda,
	seg.vdaprftabcod profissao,
	gsa.segnom nome,
	gsa.nscdat nascimento,
	gsa.cgccpfnum,
	gsa.cgccpfdig,
	gsa.segsex,
	gsa.estcvlcod,
	seg.segnaccod nacionalidade,
	seg.rsnpainom pais_resid,
	seg.peptip tipo_pep,
	seg.pepnom nome_pep,
	seg.peprelgracod grau_pep,
	seg.pepcpfnum cpf_pep, seg.pepcpfdig dig_pep,
	rge.segidtdcttip, 
	rge.segidtdctnum,
    rge.segidtdctexp, 
    rge.segidtdctexpdat
  from vnemprp prp, vnemprpseg seg, gsakseg gsa, gsaksegrge rge
 where prp.prporg = 7
   and prp.prpnumdig = 38659
   and seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig 
   and gsa.segnumdig = seg.segnumdig 
   and rge.segnumdig = seg.segnumdig 
  ;
 
-- S-E-G-U-R-A-D-O (Endereco)
-----------------------------
select
	ende.endfld,
	ende.endlgdtip,
	ende.endlgd,
	ende.endnum,
	ende.endcmp,
	ende.endbrr,
	ende.endcid,
	ende.endufd,
	ende.endcep,
	ende.endcepcmp
   from vnemprp prp, vnemprpseg seg, gsakend ende
 where prp.prporg = 7
   and prp.prpnumdig = 38659
   and seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig 
   and ende.segnumdig = seg.segnumdig
  ;

 -- S-E-G-U-R-A-D-O (Telefone)
------------------------------
select 
	tel.teltipcod,
	tel.dddnum,
	tel.telnum
   from vnemprp prp, vnemprpseg seg, gsaktel tel
 where prp.prporg = 7
   and prp.prpnumdig = 38659
   and seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig 
   and tel.segnumdig = seg.segnumdig
  ;
 
 -- S-E-G-U-R-A-D-O (Email)
---------------------------
select 
	mai.endfld,
	mai.maides
   from vnemprp prp, vnemprpseg seg, gsakendmai mai
 where prp.prporg = 7
   and prp.prpnumdig = 38659
   and seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig 
   and mai.segnumdig = seg.segnumdig
  ;
 
 select * from gsakseg ;
 select * from vtamseguro;
 select * from vnemorgsegdet;
 select * from vnemorgprpcpa where prporg = 7
   and prpnumdig = 38659;
 select * from vgakvdaseg;
 select * from vnpksegtip;
 select * from vnemprpitm;
 select * from vnpkprf;
 select segnumdig,count(1) from gsaksegrge group by segnumdig having count(1) >1;
 select segnumdig,count(1) from gsakend group by segnumdig having count(1) >1;