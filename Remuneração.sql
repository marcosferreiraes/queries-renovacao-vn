select b.*
 from vnemprp a
     ,vnemsgreds b
where a.vdasgrcod = b.vdasgrcod
  and a.prporg = b.prporg
  and a.prpnumdig = b.prpnumdig
  and a.prporg = 7
  and a.prpnumdig = 9186


-- Remuneração para integração com Antares
------------------------------------------
select distinct a.prporg,a.prpnumdig,a.remper
  from vnemprprem a
 where a.prporg = 7
   and a.prpnumdig = 40494 
    and a.remper < 100 order by a.prpnumdig desc 

-- Recuperar proposta pela apólice
------------------------------------------
select a.succod, a.aplnumdig, b.prporg, b.prpnumdig, b.emsdat
     , b.dctseqnum, b.edsnumref
  from vnemsgrapl a, vnemprp b
where --a.succod = 68 and a.aplnumdig = 126894
   b.vdasgrcod = a.vdasgrcod

-- Query realizada pelo VN para buscar remuneração Antares
----------------------------------------------------------   
   select * from vnemprpremper
select a.prporg, a.prpnumdig, b.remoprcod, b.vdaremchrcod
    , d.vdaremtip
     , d.ramcod
    , e.vdadmncntdes
    , f.corsus, f.fvrptcper
    , g.vdadmncntdes
    , h.prprempgtpper
    , h.crtprzcptincdat
 from vnemprp a, vnemprprem b, vnpkpdt c, vnptremchr d, vnpkdmncnt e
    , vnemprpremfvr f, vnpkdmncnt g, vnemprpremper h
 where a.prporg = 7 and a.prpnumdig in (27041180    )
   and b.prporg = a.prporg and b.prpnumdig = a.prpnumdig
   and c.vdapdtvercod = a.vdapdtvercod
     and d.vdaremchrcod = b.vdaremchrcod
    and d.ramcod = c.ldrramcod
     and e.vdadmncntcod = d.vdaremtip
     and e.vdadmncntrefcod = 'COR'
  and f.prpremcod    = b.prpremcod
 and f.vdaremchrcod = b.vdaremchrcod
   and g.vdadmncntcod = f.fvrtip
   and g.vdadmncntrefcod = 'COR'
   and h.prpremcod = f.prpremcod and h.prpremfvrseq = f.prpremfvrseq