-- F-O-R-M-A  D-E  E-N-V-I-O
----------------------------
select dct.dctenvmeicod, cnt.vdadmncntdes, dct.vdaocrcod, cnt1.vdadmncntdes 
  from vnprpdtsaidct dct, vnpkdmncnt cnt, vnpkdmncnt cnt1
 where dct.dctenvmeicod = cnt.vdadmncntcod
   and dct.vdaocrcod = cnt1.vdadmncntcod
 group by 1,2,3,4
 ;

select * from vnpkdct;

select qar.vdadmncntcod, cnt.vdadmncntdes, cnt.vdadmncntrefcod 
  from vnprqardmncnt qar, vnpkdmncnt cnt 
 where qar.vdadmncntcod = cnt.vdadmncntcod 
 group by qar.vdadmncntcod, cnt.vdadmncntdes, cnt.vdadmncntrefcod ;