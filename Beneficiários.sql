-- B�E�N�E�F�I�C�I���R�I�O�S
----------------------------
select
	bnf.bnfnom,
	bnf.bnfnscdat,
	bnf.cpfnumdig,
	bnf.bnfcapper,
	bnf.peptip,
	bnf.pepnom nome_pep,
	bnf.peprelgracod grau_pep,
	bnf.pepcpfnum cpf_pep, bnf.pepcpfdig dig_pep,
	cnt_grau.vdadmncntdes grau
  from vnemprpsegbnf bnf, vnpkdmncnt cnt_grau
 where bnf.prporg = 7
   and bnf.prpnumdig = 38659 
   and bnf.bnfprngracod = cnt_grau.vdadmncntcod
 ;