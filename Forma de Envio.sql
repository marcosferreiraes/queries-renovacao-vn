-- E�N�V�I�O � D�O�C�U�M�E�N�T�O
--------------------------------
select seg.endfldcod, cnt.vdadmncntrefcod, cnt.vdadmncntdes 
  from vnemprp prp, vnemprpseg seg, vnpkdmncnt cnt
 where prp.prporg = 7
   and prp.prpnumdig = 38659
   and seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig
   and seg.endfldcod = cnt.vdadmncntcod 
   ;

-- Tipos existentes de forma de envio
select seg.endfldcod, cnt.vdadmncntrefcod, cnt.vdadmncntdes 
  from vnemprp prp, vnemprpseg seg, vnpkdmncnt cnt
 where seg.prporg = prp.prporg 
   and seg.prpnumdig = prp.prpnumdig
   and seg.endfldcod = cnt.vdadmncntcod 
   --and prp.prporg = 7
   --and prp.prpnumdig = 38659
   group by 1,2,3
 ;

select
	cnt_envio.vdadmncntdes envio,
	cnt_envio.vdadmncod cod,
	cnt_envio.vdadmncntcod
  from
  	vnemprpitmgrt grt,
  	vnptpdtgrt pdt,
	vnptpdtdct dct,
	vnprpdtsaidct sai,
	vnpkgrt grt_,
	outer vnpkdmncnt cnt_evento,
	outer vnpkdmncnt cnt_envio,
	outer vnpkdmncnt cnt_dest
 where pdt.vdapdtgrtcod = grt.vdapdtgrtcod
   and pdt.vdapdtvercod = grt.vdapdtvercod
   and dct.vdapdtgrtcod = pdt.vdapdtgrtcod
   and dct.vdapdtvercod = pdt.vdapdtvercod
   and grt_.vdagrtcod = pdt.vdagrtcod
   and dct.vdapdtgrtcod = sai.vdapdtgrtcod
   and dct.vdapdtvercod = sai.vdapdtvercod
   and dct.vdaevttip = sai.vdaevttip
   and dct.vdapdtcmlmeitip = sai.vdapdtcmlmeitip
   and dct.vdasegtiptabcod = sai.vdasegtiptabcod
   and dct.vdaocrcod = sai.vdaocrcod
   and sai.dctenvmeicod = cnt_envio.vdadmncntcod
   and sai.dctdstcod = cnt_dest.vdadmncntcod
   and sai.vdaevttip = cnt_evento.vdadmncntcod
   and sai.dctenvmeicod is not null
   --and grt.prporg = 94
   --and grt.prpnumdig = 46310
group by 1,2,3
  ;

select * from vnemprp where prporg = 7 and prpnumdig = 38475;
select * from vnpkdmncnt where lower(vdadmncod) like '%tip_end%';
select ende.endfld, cnt.vdadmncntdes, cnt.vdadmncod  from gsakend ende, vnpkdmncnt cnt where ende.segnumdig = 100638042 and ende.endfld = cnt.vdadmncntcod ;
select * from gfxmcormai where cormaiseq =0;
select * from vnemgnrsegend;
select * from vnemvcrgrtcaamaidad;
select * from vnpkdct;
select * from vnemprpitmgrt;
select * from vnptpdtgrt;
select * from vnptpdtdct;
select * from vnprpdtsaidct;
select * from vnpkdmncnt where  upper(vdadmncod) like '%MEI_ENV_DOC%';
select * from vnpkdmncnt where  upper(vdadmncod) like '%TIP_END%';
select * from vnpkdmncnt where  upper(vdadmncntdes) like '%FISICO%';
select * from vnemgrtppncot;
select * from vnemppncot;
-- Dominios
select cnt.vdadmncntdes, seg.endfldcod, cnt.vdadmncntrefcod  from vnemprpseg seg, vnpkdmncnt cnt where seg.endfldcod = cnt.vdadmncntcod 
group by 1,2,3;

select b.segdctenvtipcod
  from vtamdoc a, vtamsgrcpldad b
 where b.sgrorg = a.sgrorg
   and b.sgrnumdig = a.sgrnumdig
   group by 1
 ;
 
-- Extra��o formas de envio propostas renovadas
select 
	prp.prporg,
	prp.prpnumdig,
	fvr.corsus corretor,
	gsa.segnom, gsa.cgccpfnum||gsa.cgccpfdig cpf,
	cnt.vdadmncntrefcod cod_tipo_envio,
	cnt.vdadmncntdes tipo_envio,
	mai.maides email_segurado,
	gfx.maides email_corretor,
	ende.endlgdtip tipo_logradouro,
	ende.endlgd logradouro,
	ende.endnum numero,
	ende.endcmp complemento,
	ende.endbrr bairro,
	ende.endcid cidade,
	ende.endufd uf,
	ende.endcep||'-'||ende.endcepcmp cep
  from vnemprp prp,
	vnemsgr sgr,
	vnemprpseg seg,
	vnpkdmncnt cnt,
	gsakseg gsa,
	outer gsakendmai mai,
	outer gfxmcormai gfx,
	outer gsakend ende,
	vnemprprem rem,
	vnemprpremfvr fvr, 
	vnpkdmncnt cnt_prpsit
 where seg.prporg = prp.prporg
   and seg.prpnumdig = prp.prpnumdig
   and prp.vdasgrcod = sgr.vdasgrcod
   and seg.endfldcod = cnt.vdadmncntcod
   and gsa.segnumdig = seg.segnumdig
   and gsa.segnumdig = mai.segnumdig
   --and prp.prporg = 7
   --and prp.prpnumdig = 30159
   and fvr.prpremcod = rem.prpremcod
   and prp.prporg = rem.prporg
   and prp.prpnumdig = rem.prpnumdig 
   and fvr.ldrfvrflg = 'S'
   and gfx.corsus = fvr.corsus
   and gfx.cormaiseq = 0
   and ende.segnumdig = seg.segnumdig
   and ende.endfld = 1
   and prp.prpsitcod = cnt_prpsit.vdadmncntcod
   and prp.dctvigfnldat >= "2021-12-16"
   and prp.dctvigfnldat <= "9999-12-31"
   and prp.prporg <> 94
   and cnt_prpsit.vdadmncntrefcod <> 'CAN'
   and prp.vdasgrcod not in (select prp1.vdarnvsgrcod
                                from vnemprp prp1
                                     inner join vnpkdmncnt cnt1 on
                                                (prp1.prpsitcod = cnt1.vdadmncntcod)
                               where prp.vdasgrcod = prp1.vdarnvsgrcod
                                 and prp1.prporg = 94
                                 and cnt1.vdadmncntrefcod = 'PEM')
  group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
;
   
1. nome e email do segurado e tipo envio
2. propostas onde corretor recebe com email do corretor (posteriormente ele envia p/ segurado)

