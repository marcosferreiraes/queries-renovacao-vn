-- P�A�R�C�E�L�A�S
------------------
select
	par.parnum,
	par.ramcod,
	par.parvctdat,
	cps.parcrmvlr,
	cps_iof.parcrmvlr, cps.parcrmtip 
  from vnemrampar par, vnemparcps cps, vnpkdmncnt cnt, (select * from vnemparcps) cps_iof, vnpkdmncnt cnt_iof
 where par.prporg = 7
   and par.prpnumdig = 53962
   and par.prporg = cps.prporg
   and par.prpnumdig = cps.prpnumdig
   and par.parnum = cps.parnum
   and par.ramcod = cps.ramcod
   and par.prporg = cps_iof.prporg
   and par.prpnumdig = cps_iof.prpnumdig
   and par.parnum = cps_iof.parnum
   and par.ramcod = cps_iof.ramcod
   and cps.parcrmtip = cnt.vdadmncntcod
   and cps_iof.parcrmtip = cnt_iof.vdadmncntcod
   and cnt.vdadmncntrefcod = 'LIQ'
   and cnt_iof.vdadmncntrefcod = 'IOF'
  ;

-- Testes
select * from vnemparcps cps, vnpkdmncnt cnt where cps.prpnumdig = 114587 and cps.prporg =7 and cps.parcrmtip =cnt.vdadmncntcod ;
select * from vnemrampar where prpnumdig = 114587 and prporg =7 ;
----------

select * from vnemcotpar;

-- Antares
----------
select cps.ramcod, sum(cps.parcrmvlr) 
  from vnemparcps cps, vnpkdmncnt cnt
 where cps.parcrmtip =cnt.vdadmncntcod 
   and cps.prporg =7
   and cps.prpnumdig in (29003854,29023270,28979160)
   and cnt.vdadmncntrefcod ='LIQ'
 group by cps.ramcod ;