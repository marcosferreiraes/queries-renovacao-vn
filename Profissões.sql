select * from vnpkdmncnt 
 where vdadmncntcod in (329,361,362,365,366,367,368,370,525);

 -- Primeira query executada
 ---------------------------
 select
	t2.vdaprftabcod,
	t2.vdaprfdes,
	count(1) qtde_prp
  from   vnemprpseg t1 
 inner join vnpkprf t2 on	(t1.vdaprftabcod = t2.vdaprftabcod)
group by 1,2;

-- Segunda query executada
--------------------------
 select
	t2.vdaprftabcod,
	t2.vdaprfdes,
	count(1) qtde_prp
  from  vnemprp t0 
  inner join vnemprpseg t1 on 
  (t0.prporg = t1.prporg and t0.prpnumdig = t1.prpnumdig)
  inner join vnpkprf t2 on	(t1.vdaprftabcod = t2.vdaprftabcod)
  inner join vnpkdmncnt cnt on (t0.prpsitcod = cnt.vdadmncntcod)
 where cnt.vdadmncntrefcod = 'PEM'
   and t0.dctvigfnldat >= "2021-12-16"
   and t0.dctvigfnldat <= "9999-12-31"
group by 1,2;

-- Terceira query executada
---------------------------
 select
	t2.vdaprftabcod,
	t2.vdaprfdes,
	count(1) qtde_prp
   from vnemprp prp
        inner join vnemsgr sgr on (prp.vdasgrcod = sgr.vdasgrcod)
        inner join vnemprpseg seg on 
        (prp.prporg = seg.prporg and prp.prpnumdig = seg.prpnumdig)
        inner join vnpkprf t2 on	(seg.vdaprftabcod = t2.vdaprftabcod)
        inner join vnpkdmncnt cnt on (prp.prpsitcod = cnt.vdadmncntcod)
  where prp.dctvigfnldat >= "2021-12-16"
    and prp.dctvigfnldat <= "9999-12-31"
    and cnt.vdadmncntrefcod <> 'CAN'
    and prp.vdasgrcod not in (select prp1.vdarnvsgrcod
                                from vnemprp prp1
                                     inner join vnpkdmncnt cnt1 on
                                                (prp1.prpsitcod = cnt1.vdadmncntcod)
                               where prp.vdasgrcod = prp1.vdarnvsgrcod
                                 and prp1.prporg = 94
                                 and cnt1.vdadmncntrefcod = 'PEM')
  group by 1,2;
  
 -- Detalhamento Propostas x Profiss�o
 -------------------------------------
  select
	prp.prporg, prp.prpnumdig, prp.dctvigfnldat, prp.prpsitcod, cnt.vdadmncntrefcod,
	t2.vdaprftabcod, t2.vdaprfdes
   from vnemprp prp
        inner join vnemsgr sgr on (prp.vdasgrcod = sgr.vdasgrcod)
        inner join vnemprpseg seg on 
        (prp.prporg = seg.prporg and prp.prpnumdig = seg.prpnumdig)
        inner join vnpkprf t2 on	(seg.vdaprftabcod = t2.vdaprftabcod)
        inner join vnpkdmncnt cnt on (prp.prpsitcod = cnt.vdadmncntcod)
  where prp.dctvigfnldat >= "2021-12-16"
    and prp.dctvigfnldat <= "9999-12-31"
    and t2.vdaprftabcod in (1178,2016,950,1196)
    and cnt.vdadmncntrefcod <> 'CAN'
    and prp.vdasgrcod not in (select prp1.vdarnvsgrcod
                                from vnemprp prp1
                                     inner join vnpkdmncnt cnt1 on
                                                (prp1.prpsitcod = cnt1.vdadmncntcod)
                               where prp.vdasgrcod = prp1.vdarnvsgrcod
                                 and prp1.prporg = 94
                                 and cnt1.vdadmncntrefcod = 'PEM');